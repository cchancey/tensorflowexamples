/**
 * @file TensorflowRewrapper.hpp
 * @date Created on: Nov 29, 2018
 * @author developer
 * @brief 
 */

#ifndef PROJECTS_INCLUDE_TENSORFLOWREWRAPPER_HPP_
#define PROJECTS_INCLUDE_TENSORFLOWREWRAPPER_HPP_

#include <fstream>
#include <utility>
#include <vector>
#include <boost/filesystem.hpp>
#include <gsl/gsl>

#include "tensorflow/cc/ops/nn_ops.h"
#include "tensorflow/cc/ops/const_op.h"
#include "tensorflow/cc/ops/image_ops.h"
#include "tensorflow/cc/ops/standard_ops.h"
#include "tensorflow/core/framework/graph.pb.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/graph/default_device.h"
#include "tensorflow/core/graph/graph_def_builder.h"
#include "tensorflow/core/lib/core/errors.h"
#include "tensorflow/core/lib/core/stringpiece.h"
#include "tensorflow/core/lib/core/threadpool.h"
#include "tensorflow/core/lib/io/path.h"
#include "tensorflow/core/lib/strings/str_util.h"
#include "tensorflow/core/lib/strings/stringprintf.h"
#include "tensorflow/core/platform/env.h"
#include "tensorflow/core/platform/init_main.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/public/session.h"
#include "tensorflow/core/util/command_line_flags.h"
#include "tensorflow/cc/saved_model/loader.h"
#include "tensorflow/cc/saved_model/tag_constants.h"
#include "tensorflow/cc/saved_model/constants.h"
#include "tensorflow/cc/saved_model/signature_constants.h"
#include "tensorflow/cc/saved_model/tag_constants.h"

inline void status_check(const tensorflow::Status stat){
  if(stat!=tensorflow::Status::OK()){
    throw std::runtime_error(stat.ToString());
  }
}

inline tensorflow::Status LoadGraph(const std::string& graph_file_name,
                 std::unique_ptr<tensorflow::Session>* session) {
  tensorflow::GraphDef graph_def;
  status_check(
      ReadBinaryProto(tensorflow::Env::Default(), graph_file_name, &graph_def));

  session->reset(tensorflow::NewSession(tensorflow::SessionOptions()));
  status_check((*session)->Create(graph_def));

  return tensorflow::Status::OK();
}

class graph{
 public:
  graph(const std::string& model_location,const std::string& input_layer, const std::vector<std::string>& output_layers):input_layer_{input_layer},output_layers_{output_layers}{
    status_check(LoadGraph(model_location,&session));
  }
  tensorflow::Tensor run(const tensorflow::Tensor& input_tensor)const{
    std::vector<tensorflow::Tensor> outputs;
    status_check(
        session->Run({{input_layer_, input_tensor}}, output_layers_, {},
                     &outputs));
    return outputs.at(0);
  }
 private:
  std::string input_layer_;
  std::vector<std::string> output_layers_;
  std::unique_ptr<tensorflow::Session> session;
};



#endif /* PROJECTS_INCLUDE_TENSORFLOWREWRAPPER_HPP_ */
