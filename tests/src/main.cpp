#include<TensorflowRewrapper.hpp>

// These are all common classes it's handy to reference with no namespace.
using tensorflow::Flag;
using tensorflow::Tensor;
using tensorflow::Status;
using tensorflow::string;
using tensorflow::int32;

void printTensor(const Tensor &t) {
  std::cout << "Dims:" << t.dims() << std::endl;
  std::cout << "Dtype:" << t.dtype() << std::endl;
  std::cout << t.DebugString() << std::endl;
  std::cout << "End print tensor" << std::endl;
}

void ReadEntireFile(tensorflow::Env* env, const string& filename,
                    Tensor* output) {
  tensorflow::uint64 file_size = 0;
  status_check(env->GetFileSize(filename, &file_size));

  string contents;
  contents.resize(file_size);

  std::unique_ptr<tensorflow::RandomAccessFile> file;
  status_check(env->NewRandomAccessFile(filename, &file));

  tensorflow::StringPiece data;
  status_check({file->Read(0, file_size, &data, &(contents)[0])});
  Ensures(data.size() == file_size);
  output->scalar<string>()() = std::string(data);
}

void RunOpByPointer(
    const tensorflow::Scope& root, const std::string & op_name,
    std::vector<tensorflow::Tensor>* output,
    const std::vector<std::pair<std::string, tensorflow::Tensor>>& inputs) {

  tensorflow::GraphDef graph;
  status_check(root.ToGraphDef(&graph));
  std::unique_ptr<tensorflow::Session> sess(
      tensorflow::NewSession(tensorflow::SessionOptions()));
  status_check(sess->Create(graph));
  status_check(sess->Run(inputs, {op_name}, {}, output));
}

void FormattingOps(tensorflow::Scope &root,
                   const tensorflow::Output &file_reader,
                   const tensorflow::int32 &height,
                   const tensorflow::int32 &width, const float input_std) {

  const auto float_caster{tensorflow::ops::Cast(root.WithOpName("float_caster"),
                                                file_reader,
                                                tensorflow::DT_FLOAT)};
  const auto dims_expander{tensorflow::ops::ExpandDims(root, float_caster, 0)};
  const auto resized{tensorflow::ops::ResizeBilinear(
      root, dims_expander, tensorflow::ops::Const(root.WithOpName("size"), {
                                                      height, width}))};
  tensorflow::ops::Div(root.WithOpName(std::string{"normalized"}), resized, {input_std});
}


//todo breakup
Status ReadTensorFromImageFile(const std::string& file_name,
                               const int input_height, const int input_width,
                               const float input_mean, const float input_std,
                               std::vector<Tensor>* out_tensors) {
  auto root = tensorflow::Scope::NewRootScope();
  using namespace ::tensorflow::ops;

  string input_name = "file_reader";
  string output_name = "normalized";

  // read file_name into a tensor named input
  Tensor input(tensorflow::DT_STRING, tensorflow::TensorShape());

  ReadEntireFile(tensorflow::Env::Default(), file_name, &input);

  // use a placeholder to read input data
  auto file_reader = Placeholder(root.WithOpName("input"),
                                 tensorflow::DataType::DT_STRING);

  std::vector<std::pair<string, tensorflow::Tensor>> inputs =
      {{"input", input}, };
  // Now try to figure out what kind of file it is and decode it.
  const int wanted_channels = 3;
  tensorflow::Output image_reader;
  // Assume if it's neither a PNG nor a GIF then it must be a JPEG.
  image_reader = DecodeJpeg(root.WithOpName("jpeg_reader"), file_reader,
                            DecodeJpeg::Channels(wanted_channels));
  FormattingOps(root,image_reader,input_height,input_width,input_std);
  RunOpByPointer(root,"normalized",out_tensors,inputs);
  return Status::OK();
}

int main(int argc, char* argv[]) {

  string graph_path = "./tensorflowmodels/Dakoda/logo_model.pb";
  int32 input_width = 224;
  int32 input_height = 224;
  float input_mean = 0;
  float input_std = 255;
  string input_layer = "init_input_layer_1:0";
  string output_layer = "dense_16_1/Softmax:0";
  std::string warmup_directory = "./warmupimages/";
  std::string directory_path = "./testimages/";

//  string graph_path =
//      "/choke_models/logo_model.pb";
//  int32 input_width = 224;
//  int32 input_height = 224;
//  float input_mean = 0;
//  float input_std = 255;
//  string input_layer = "init_input_layer_1:0";
//  string output_layer = "dense_16_1/Softmax:0";
//  std::string warmup_directory="/choke_models/warmup/";
//  std::string directory_path="/choke_models/samples/";

// We need to call this to set up global state for TensorFlow.
  tensorflow::port::InitMain(argv[0], &argc, &argv);

  graph my_graph(graph_path, input_layer, {output_layer});

  std::vector<Tensor> resized_tensors;
  for (const boost::filesystem::directory_entry & file : boost::filesystem::directory_iterator(
      warmup_directory)) {
    status_check(
        {ReadTensorFromImageFile(file.path().string(), input_height,
                                 input_width, input_mean, input_std,
                                 &resized_tensors)});
    my_graph.run(resized_tensors[0]);
  }

  tensorflow::int64 total_time = 0;
  int count = 0;
  for (const boost::filesystem::directory_entry & file : boost::filesystem::directory_iterator(
      directory_path)) {
    status_check(
        {ReadTensorFromImageFile(file.path().string(), input_height,
                                 input_width, input_mean, input_std,
                                 &resized_tensors)});
    tensorflow::int64 start_time = tensorflow::Env::Default()->NowMicros();
    auto output = my_graph.run(resized_tensors[0]);
    tensorflow::int64 end_time = tensorflow::Env::Default()->NowMicros();
    total_time += end_time - start_time;
    std::cout << "Prediction time:" << end_time - start_time << std::endl;
    printTensor(output);
    count++;
  }
  std::cout << "Total time:" << total_time << " Average: "
            << (total_time / count) << std::endl;

  return 0;
}
