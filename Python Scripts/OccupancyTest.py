import cv2 as cv
import numpy as np
import keras 
import h5py
from keras.utils.generic_utils import CustomObjectScope
import tensorflow as tf

img1=cv.imread('dog.jpeg',3)# 3 dimensional 
img2=cv.imread('dog.jpeg',1)# 3 dimensional 
data = []
data.append(img1)
data.append(img2)

jpeg_string=tf.read_file('dog.jpeg')
tfImage=tf.image.decode_jpeg(jpeg_string,3) 
with tf.Session() as sess:
  tftemp=tfImage.eval()


with CustomObjectScope({'relu6': keras.applications.mobilenet.relu6,'DepthwiseConv2D': keras.applications.mobilenet.DepthwiseConv2D}):
	previous_model=keras.models.load_model('Occupancy.hdf5')

#prediction=previous_model.predict(np.array([img1])) #4 Dimensional 
prediction=previous_model.predict(np.array([tftemp]))
print (prediction)
