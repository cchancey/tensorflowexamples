import tensorflow as tf
import tensorflow.python.saved_model
import cv2 as cv
import numpy as np
from tensorflow.contrib import predictor

export_path ="./weights.pb"
with tf.gfile.GFile(export_path, "rb") as f:
    restored_graph_def = tf.GraphDef()
    restored_graph_def.ParseFromString(f.read())

with tf.Graph().as_default() as graph:
    tf.import_graph_def(
        restored_graph_def,
        input_map=None,
        return_elements=None,
        name=""
        )

y=graph.get_tensor_by_name("sequential_2/dense_100_1/Sigmoid:0")
x=graph.get_tensor_by_name("input_1_1:0")
img1=cv.imread('dog.jpeg',3)# 3 dimensional 
img2=cv.imread('dog.jpeg',1)# 3 dimensional 
data = []
data.append(img1)
data.append(img2)

sess= tf.Session(graph=graph)
inputs={x:(np.array([img1]))} #4 Dimensional 
predictions=sess.run(y,inputs)
print (predictions)
