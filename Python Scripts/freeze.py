import tensorflow as tf
from tensorflow import train
from tensorflow import gfile
from tensorflow import graph_util


saver = tf.train.import_meta_graph('./weights.meta', clear_devices=True)
graph = tf.get_default_graph()
input_graph_def = graph.as_graph_def()
sess = tf.Session()
saver.restore(sess, "./weights")
output_node_names="sequential_2/dense_100_1/Sigmoid"
print()
output_graph_def = graph_util.convert_variables_to_constants(
            sess, # The session
            input_graph_def, # input_graph_def is useful for retrieving the nodes 
            output_node_names.split(",")  
)
output_graph="./weights.pb"
with tf.gfile.GFile(output_graph, "wb") as f:
    f.write(output_graph_def.SerializeToString())
 
sess.close()
