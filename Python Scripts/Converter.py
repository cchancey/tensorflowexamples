import keras 
import h5py
from keras.utils.generic_utils import CustomObjectScope
import tensorflow as tf
from keras import backend as K
from tensorflow.python.saved_model import builder as saved_model_builder

server = tf.train.Server.create_local_server()
sess = tf.Session(server.target)
K.set_session(sess)
K._LEARNING_PHASE = tf.constant(0)
K.set_learning_phase(0)

with CustomObjectScope({'relu6': keras.applications.mobilenet.relu6,'DepthwiseConv2D': keras.applications.mobilenet.DepthwiseConv2D}):
	previous_model=keras.models.load_model('Occupancy.hdf5')

# serialize the model and get its weights, for quick re-building
json_model=previous_model.to_json()
weights = previous_model.get_weights()

# re-build a model where the learning phase is now hard-coded to 0
from keras.models import model_from_config
with CustomObjectScope({'relu6': keras.applications.mobilenet.relu6,'DepthwiseConv2D': keras.applications.mobilenet.DepthwiseConv2D}):
	new_model = keras.models.model_from_json(json_model)
	new_model.set_weights(weights)


from tensorflow.python.saved_model import builder as saved_model_builder
from tensorflow.python.saved_model import tag_constants, signature_constants, signature_def_utils_impl
from tensorflow import graph_util


export_path ="/home/cameronchancey/kersasify/exports/freeze1.1/"# where to save the exported graph
builder = saved_model_builder.SavedModelBuilder(export_path)

signature = tf.saved_model.signature_def_utils.predict_signature_def(
    inputs={'input': new_model.inputs[0]},
    outputs={'output': new_model.outputs[0]})


with K.get_session() as sess:
    builder.add_meta_graph_and_variables(
        sess=sess,
        tags=[tag_constants.SERVING],
        clear_devices = True,
        signature_def_map={
            signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: signature}
    )	
    g=sess.graph
    gdef=g.as_graph_def()

    saver=tf.train.Saver()
    saver.save(sess, export_path+'weights')
    tf.train.write_graph(sess.graph, export_path, 'mymodel.pbtxt')
    tf.train.write_graph(sess.graph, export_path, 'saved_model2.pb',False)
builder.save()
